import { IRouterContext } from './../core/IRouterContext';
import dingTalkApi from './../node_modules/dingtalk-app/dtalk/DingTalkApi';
import * as Router from 'koa-router';
import * as jwt from 'jsonwebtoken'
import env from "./../config/env";
import dTalkApiUtil from './../core/lib/util/dTalkApiUtils';
let router = new Router();
router.prefix('/api/auth');
router.get('/login', async (ctx: IRouterContext, next: any) => {
    let authCode: string = ctx.query.code;
    let authjson = await dingTalkApi.login(authCode, 'mobile', 'isAdmin', 'roles', 'avatar', 'department', 'isLeaderInDepts');
    ctx.body = authjson;
    await next();
});

router.get('/getPcJsConfig', async (ctx: IRouterContext, next: any) => {

    let config = await dingTalkApi.signature('', env.pcmain + (ctx.querystring ? `?${ctx.querystring}` : ""));
    ctx.body = config
    await next();
});

router.get('/getJsConfig', async (ctx: IRouterContext, next: any) => {
    let config = await dingTalkApi.signature(ctx.querystring?'?' + ctx.querystring:"");
    ctx.body = config
    await next();
});

export default router