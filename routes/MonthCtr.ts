import * as Router from 'koa-router';
import { IRouterContext } from '../core/IRouterContext';
import monthService from "./../service/MonthService";
let router = new Router();
let Multer = require('koa-multer');
let multer = Multer({});
let xlsx = require('node-xlsx');
router.prefix('/api/finance')

//报表导入
router.post('/importFile', multer.single('file'), async (ctx: IRouterContext, next) => {
    let authorization = ctx.request.header.authorization;
    let date = ctx.request.header.month ;
    let excel = xlsx.parse(ctx.req.file.buffer);
    let data :any = excel[0];

    let result = await monthService.importExcel(data,authorization,date);
    ctx.status = 200 ;
    ctx.body = result ;
    await next();
})

// 获取log日志
router.post('/getLogList', async (ctx: IRouterContext, next) => {
    let res = ctx.request.body ;
    let result = await monthService.getLogList(res);
    ctx.status = 200 ;
    ctx.body = result ;
    await next();
})

//获取仪器列表
router.post('/queryIns', async (ctx: IRouterContext, next) => {
    let res = ctx.request.body ;
    let result = await monthService.queryIns(res);
    ctx.status = 200 ;
    ctx.body = result ;
    await next();
})

//获取合作列表
router.post('/queryCoo', async (ctx: IRouterContext, next) => {
    let res = ctx.request.body ;
    let result = await monthService.queryCoo(res);
    ctx.status = 200 ;
    ctx.body = result ;
    await next();
})

//获取权限列表
router.post('/queryRoleList', async (ctx: IRouterContext, next) => {
    let result = await monthService.queryRoleList();
    ctx.status = 200 ;
    ctx.body = result ;
    await next();
})

//添加权限
router.post('/createRole', async (ctx: IRouterContext, next) => {
    let res :any = ctx.request.body ;
    let result = await monthService.createRole(res);
    ctx.status = 200 ;
    ctx.body = result ;
    await next();
})

//根据userId 删除 role
router.post('/deleteRoleByUserId', async (ctx: IRouterContext, next) => {
    let res :any = ctx.request.body ;
    let result = await monthService.deleteRoleByUserId(res);
    ctx.status = 200 ;
    ctx.body = result ;
    await next();
})

router.post('/editIsEnable', async (ctx: IRouterContext, next) => {
    let res :any = ctx.request.body ;
    let result = await monthService.editIsEnable(res);
    ctx.status = 200 ;
    ctx.body = result ;
    await next();
})

//判断权限
router.post('/getRole', async (ctx: IRouterContext, next) => {
    let authorization = ctx.request.header.authorization;
    let result = await monthService.getRole(authorization);
    ctx.status = 200 ;
    ctx.body = result ;
    await next();
})



export default router;