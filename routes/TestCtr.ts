import * as Router from 'koa-router';
import { IRouterContext } from '../core/IRouterContext';
import testService from "./../service/TestService";
let router = new Router();
router.prefix('/api/test')

//1当前用户为权限最大，查看所有得负责人
router.get('/test', async (ctx: IRouterContext, next: any) => {
    console.log('进ru__________________',);
    let res = ctx.request.body;
    let result = await testService.test(res);
    ctx.body = result;
    ctx.status = 200;
    await next();
})

router.get('/create',async (ctx : IRouterContext , next :any ) => {
    let result = await testService.createDepartment();
    ctx.body = result ;
    ctx.status = 200 ;
    await next();
})

export default router;