import * as Router from 'koa-router';
import * as Koa from 'koa';
import * as http from "http";
export interface IRouterContext extends Router.IRouterContext {
    session: Session;
    request: Request;
    req: Req;
    render(m: string, error: any): void;
    header: Header;
}
export interface Req extends http.IncomingMessage {
    file: any;
    files: any;
    body: any;
}

export interface Request extends Koa.Request {
    body: any;

}
export interface Session {
    id: string;

}
export interface Header {
    authorization: string;
    corpid: string;
}