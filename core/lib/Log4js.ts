let log4js = require('koa-log4');
/**
 * Log4js
 */
class Log4js {
    private _error: Logger = new Logger('log_error');
    private _info: Logger = new Logger('log_info');

    errorLogger(): Logger {
        return this._error;
    }
    infoLogger(): Logger {
        return this._info;
    }

}

/**
 * Log4js
 */
class Logger {
    _log4js: any;
    constructor(loggerName: string) {

        this._log4js = log4js.getLogger(loggerName);
    }
    error(...msg: any[]) {
        console.log(msg);
        this._log4js.error(msg);
    }
    info(...msg: any[]) {
        console.log(msg);
        this._log4js.info(msg);
    }
    debug(...msg: any[]) {
        console.log(msg);
        this._log4js.debug(msg);
    }
}

let logger: Log4js = new Log4js();
export default logger;