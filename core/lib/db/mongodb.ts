import evn from './../../../config/env';
import * as mongoose from 'mongoose';
//mongoose.Promise = global.Promise;  

let db = mongoose.createConnection(evn.dbUrl, { user: evn.dbUser, pass: evn.dbPass });
// let db = mongoose.createConnection('localhost', 'cloud_guest');
// let db = mongoose.createConnection('mongodb://123123a@dds-bp15a0eb0d6096c42.mongodb.rds.aliyuncs.com:3717,dds-bp15a0eb0d6096c41.mongodb.rds.aliyuncs.com:3717/admin?replicaSet=mgset-2959659');

db.on('error', console.error.bind(console, '连接错误:'));
db.once('open', function () {
    //一次打开记录
    console.log('数据库被打开.........');

});
export default db; 