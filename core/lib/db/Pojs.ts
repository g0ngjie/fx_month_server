export interface Pojo {
    id: number
    createdAt: Date
    updatedAt: Date
    timestamps: Date
    deletedAt: Date
}