export interface UserInfoVo {
    userid: string;
    sys_level: number;
    errmsg: string;
    is_sys: boolean;
    deviceId: string;
    errcode: number;
}