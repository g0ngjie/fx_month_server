import evn from './env';
let logPath = evn.logPath;
const json: any = {
    "appenders":
    [
        {
            "category": "console",
            "type": "console"
        },
        {
            "category": "log_info",
            "type": "file",
            "filename": logPath + "/log_info/info.log",
            "maxLogSize": 104857500,
            "backups": 100
        },
        {
            "category": "access",
            "type": "dateFile",
            "filename": logPath + "/access/access",
            "alwaysIncludePattern": true,
            "pattern": "-yyyy-MM-dd-hh.log"
        },
        {
            "category": "log_error",
            "type": "dateFile",
            "filename": logPath + "/log_error/error",
            "alwaysIncludePattern": true,
            "pattern": "-yyyy-MM-dd-hh.log"

        }
    ],
    "replaceConsole": true,
    "levels":
    {
        "log_info": "ALL",
        "log_error": "ERROR",
        "access": "ALL",
    }
};
export default json;