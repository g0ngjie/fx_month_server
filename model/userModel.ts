import * as mongoose from 'mongoose';
import db from './../core/lib/db/mongodb';

// 用户 实体类
let userSchema: mongoose.Schema = new mongoose.Schema(
    {   
        principal : String ,
        principalId : String ,
        principalJob : String ,
        manager : String ,
        managerId : String ,
        managerJob : String
    }, { collection: 'sys_user', timestamps: { createdAt: 'createDate', updatedAt: 'updateDate' } });
export interface SysUser extends mongoose.Document {
    principal :string ;
    principalId : string ;
    principalJob : string ;
    manager : string ;
    managerId :string ;
    managerJob : string ;
}
export let userModel = db.model<SysUser>('sys_user', userSchema);
