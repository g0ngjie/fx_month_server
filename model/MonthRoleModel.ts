import * as mongoose from 'mongoose';
import db from './../core/lib/db/mongodb';

// 用户 权限类
let monthRoleSchema: mongoose.Schema = new mongoose.Schema(
    {   
        userId : String ,
        userName : String ,
        avatar : String ,
        isEnable : Number 

    }, { collection: 'month_role', timestamps: { createdAt: 'createDate', updatedAt: 'updateDate' } });
export interface MonthRole extends mongoose.Document {
    userId : string ;
    userName : string ;
    avatar : string ;
    isEnable : number ;
}
export let monthRoleModel = db.model<MonthRole>('month_role', monthRoleSchema);
