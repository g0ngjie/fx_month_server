import * as mongoose from 'mongoose';
import db from './../core/lib/db/mongodb';

// log 日志
let syslogSchema: mongoose.Schema = new mongoose.Schema(
    {   
        userName : String ,
        detail : String ,
        action : String 

    }, { collection: 'sysLog', timestamps: { createdAt: 'createDate', updatedAt: 'updateDate' } });
export interface SysLog extends mongoose.Document {
    userName : string ;
    detail : string ;
    action : string ;
}
export let sysLogModel = db.model<SysLog>('sysLog', syslogSchema);
