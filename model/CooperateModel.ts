import * as mongoose from 'mongoose';
import db from './../core/lib/db/mongodb';

// 按合作模式投放报表
let cooperateSchema: mongoose.Schema = new mongoose.Schema(
    {   
        typeName : String ,       //合作类型
        num : String ,            //台数
        monthTask : String ,      //本月任务
        monthDone : String ,      //本月完成
        doneRate : String ,       //完成率
        momDone : String ,        //环比完成     
        momRate : String ,        //环比率
        samePeriodTask : String , //同期任务
        samePeriodDone : String , //同期完成
        samePeriodRate : String , //同期完成率
        cumulativeTask : String , //累计任务
        cumulativeDone : String , //累计完成
        cumulativeRate : String , //累计完成率
        yearMonth : String        //年月

    }, { collection: 'cooperate', timestamps: { createdAt: 'createDate', updatedAt: 'updateDate' } });
export interface Cooperate extends mongoose.Document {
    typeName : string ;    
    num : string ;
    monthTask : string ;  
    monthDone : string ;
    doneRate : string ;
    momDone : string ;
    momRate : string ;
    samePeriodTask : string ;
    samePeriodDone : string ;
    samePeriodRate : string ;
    cumulativeTask : string ;
    cumulativeDone : string ;
    cumulativeRate : string ;
    yearMonth : string ;
}
export let cooperateModel = db.model<Cooperate>('cooperate', cooperateSchema);
